const socket = io("/");

function sendMessage(message){
    socket.emit("newMessage", { message });
}

function handleNotification(data){
    const {message} = data;
    console.log(`somebody said ${message}`);
}

socket.on("messageNotification", handleNotification);